package leetocode.wc109;

import java.math.BigInteger;
import java.util.*;

public class KnightDialer {


    public static void main(String[] args) {
        //System.out.println(knightDialer(1));
        System.out.println(knightDialer(2));
    }

    static public int knightDialer(int N) {
        int[][] board = new int[][]{
                {5, 7}, //1
                {6, 8}, //2
                {3, 7}, //3
                {2, 8, 9},// 4
                {-1, -1}, //5
                {0, 6, 9}, //6
                {1, 5},//7
                {0, 2}, //8
                {3, 1}, // 9 -> 0
                {3, 5} // 10

        };

        BigInteger res = BigInteger.ZERO;
        BigInteger[][] cache = new BigInteger[10][N];
        List<String> numbers = new ArrayList<>();
        for (int i = 0; i < board.length; i++) {
            res = res.add(dfs(i, board, cache, N - 1, String.valueOf(getDialDigit(i)), numbers, N));
        }

        System.out.println(numbers);
        System.out.println("ops: " + numbers.size());
        return res.mod(BigInteger.valueOf((long) Math.pow(10, 9) + 7L)).intValue();
    }


   static int getDialDigit(int i) {
        int j = i + 1;
        if(j == 10) return 0;
        else return j;
    }

    static BigInteger dfs(int root, int[][] board, BigInteger[][] cache, int jumps, String number, List<String> numbers, int N) {
        if (jumps == 0) {
            numbers.add(number);
            return BigInteger.ONE;
        }
        BigInteger res = BigInteger.ZERO;
        if (cache[root][jumps] != null) return cache[root][jumps];
        if (cache[root][N - jumps] != null) return cache[root][N - jumps];
        for (int adjacent : board[root]) {
            if (adjacent != -1) {
                res = res.add(dfs(adjacent, board, cache, jumps - 1, number + "-" + getDialDigit(adjacent), numbers, N));
            }
        }
        cache[root][jumps] = res;
        cache[root][N - jumps] = res;
        //cache[root]
        return res;
    }


    static int[][] GRAPH = new int[][]{
            {5, 7}, //1
            {6, 8}, //2
            {3, 7}, //3
            {2, 8, 9},// 4
            {-1, -1}, //5
            {0, 6, 9}, //6
            {1, 5},//7
            {0, 2}, //8
            {3, 1}, // 9
            {3, 5} // 0

    };

    static BigInteger exp = BigInteger.valueOf((long)Math.pow(10, 9) + 7L);

    static int useDp(int N) {
        BigInteger[][] dp = new BigInteger[N][10];

        for (int i = 0; i < 10; i++) {
            dp[0][i] = BigInteger.ONE;
        }

        for (int i = 1; i < N; i++) {
            for (int j = 0; j < 10; j++) {
                dp[i][j] = BigInteger.ZERO;
                for (int k : GRAPH[j]) {
                    if (k != -1) {
                        dp[i][j] = dp[i][j].add(dp[i - 1][k]);
                        if(dp[i][j].longValue() >= Integer.MAX_VALUE) {
                            dp[i][j] = dp[i][j].mod(exp);
                        }
                    }
                }
               // dp[i][j] = dp[i][j].mod(exp);
            }
        }
        BigInteger ans = BigInteger.ZERO;
        for (int i = 0; i < 10; i++) {
            ans = ans.add(dp[N - 1][i]);
        }
        return ans.mod(exp).intValue();
    }
}
