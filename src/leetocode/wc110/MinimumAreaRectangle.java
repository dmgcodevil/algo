package leetocode.wc110;

import java.util.*;

//   https://leetcode.com/problems/minimum-area-rectangle/
public class MinimumAreaRectangle {

    public static void main(String[] args) {

        System.out.println((int) (40000 * 40000));

        System.out.println(40000L * 40000L);

        int[][] data = new int[][]
                {
                        {1, 1},
                        {1, 3},
                        {3, 1},
                        {3, 3},
                        {2, 2}
                };

//int[][] data = new int[][] {{1,1},{1,3},{1,4},{2,3},{2,4},{4,1},{4,4}};
        //     int[][] data = new int[][]{{1, 1}, {1, 3}, {1, 4}, {4, 3}, {4, 1}, {4, 4}};
        System.out.println(minAreaRect(data));
    }


    // this is my ugly solution
    // idea: sort by first coordinate point[0]
    // remove duplicates, example: 2,1,3,1,3 -> 1,2,3
    // tree set service this purpose
    // next we do n^2 operations trying all possibler pairs of x coordinates: [x1=1,x2=2], [x1=1,x2=3] and etc.
    // once we have x1 and x2 it forms a bottom of the possible triangle
    // so far so good
    // now we need to find two pairs of `y` coordinates for x1 and x2, i.e. :
    // x1 -> (y1, y2)
    // x2 -> (y1', y2')
    // such that y1 == y1' and y2 == y2'

    // brute force iterate through all possible pairs
    // improvement: consider only closest points, i.e. 3,4 should go before 1,3 and etc.
    public static int minAreaRect(int[][] points) {


        Integer ans = Integer.MAX_VALUE;
        Map<Integer, Set<Integer>> x_to_y = new HashMap<>();
        Set<Integer> coll = new TreeSet<>();
        for (int[] point : points) {
            add(point[0], x_to_y, point[1]);
            coll.add(point[0]);
        }

        Integer[] x_points = coll.toArray(new Integer[0]);


        for (int i = 0; i < x_points.length; i++) {
            for (int j = i + 1; j < x_points.length; j++) {
                Integer[] x1_points = x_to_y.get(x_points[i]).toArray(new Integer[0]);
                Set<Integer> x2_points = x_to_y.get(x_points[j]);
                for (int k = 0; k < x1_points.length; k++) {
                    if (x2_points.contains(x1_points[k])) {
                        // we found first point
                        for (int l = k + 1; l < x1_points.length; l++) {
                            if (x2_points.contains(x1_points[l])) {
                                // found second bitch
                                int bottom = x_points[j] - x_points[i];
                                int top = x1_points[l] - x1_points[k];
                                ans = Math.min(top * bottom, ans);
                                break;
                            }
                        }
                    }
                }

            }
        }

        return Integer.MAX_VALUE == ans ? 0 : ans;
    }


    static void add(int k, Map<Integer, Set<Integer>> m, int v) {
        if (!m.containsKey(k)) {
            m.put(k, new TreeSet<>());
        }
        m.get(k).add(v);
    }
}
