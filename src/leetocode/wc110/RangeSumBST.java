package leetocode.wc110;

import leetocode.common.TreeNode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class RangeSumBST {

    public int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) return 0;
        int sum = 0;
        Stack<TreeNode> stack = new Stack<>();
        Set<Integer> visited = new HashSet<>();
        stack.add(root);
        visited.add(root.val);
        sum = between(root.val, L, R) ? root.val : 0;

        while (!stack.isEmpty()) {
            TreeNode node = stack.peek();
            if (node.left != null && !visited.contains(node.left.val)) {
                visited.add(node.left.val);
                stack.add(node.left);
                sum += between(node.left.val, L, R) ? node.left.val : 0;
            } else if (node.right != null && !visited.contains(node.right.val)) {
                visited.add(node.right.val);
                stack.add(node.right);
                sum += between(node.right.val, L, R) ? node.right.val : 0;
            } else stack.pop();

        }
        return sum;
    }

    static boolean between(int val, int L, int R) {
        return val >= L && val <= R;
    }


    int req(TreeNode root, int L, int R) {
        int sum = between(root.val, L, R) ? root.val : 0;
        if (root.left != null) {
            sum += req(root.left, L, Math.min(root.val, R));
        }
        if (root.right != null) {
            sum += req(root.right, Math.max(root.val, L), R);
        }
        return sum;
    }
}
